#pragma once

#include <stdio.h>
#include <iostream>
using namespace std;

class JustDoIt
{
public:
	JustDoIt();
	~JustDoIt();

	void print();

	//std::ostream& operator<<(const JustDoIt&   c);
	friend ostream& operator<<(ostream& os, const JustDoIt& dt);

	
private:

	void usesome();

	int m_tre;
	float m_gre;
	char m_char;

};

