// main.cpp
//#include "targetver.h"

#include <SDKDDKVer.h>

#include <stdio.h>

#include <cassert>
#include <tchar.h>
#include <string>
#include <vector>
#include <map>

#include <fstream>
#include <sstream>
#include <iostream>

#include "SingelLinkedList.h"
#include "SearchTree.h"
#include "DoubleLinkedList.h"
#include "JustDoIt.h"

#include "Unit.hpp"

#include "vld.h"

template <class TKey, class TValue>
struct KeyValuePair
{
	TKey key;
	TValue value;
};

template <class TKey>
struct KeyValuePair<TKey, std::string>
{
	TKey key;
	std::string value;
};

template <class TKey>
struct KeyValuePair < TKey, float >
{
	TKey key;
	float value;
};

template <class TValue>
struct KeyValuePair < float, TValue >
{
	float key;
	TValue value;
};

template <>
struct KeyValuePair < float, float >
{
	float key;
	float value;
};


struct String
{
	String(const char* string)
	{
		int len = strlen(string);
		m_string = new char[len + 1];
		strcpy_s(m_string, 32 , string);
	};

	~String()
	{

	};
	char* m_string;
};


void testSingelwithInt()//feel free to add more tests
{
	SingelLinkedList<int> list;

	verify(0, list.size(), " Size  | empty ");
	
	list.push_back(1);
	verify(1, *list.at(0), " First element  | 1 ");
	verify(1, list.size(), " Size  | 1 ");

	list.push_front(16);
	verify(16, *list.at(0), " First element  | 16 ");

	list.push_back(12);
	list.push_back(22);
	list.push_back(32);
	list.push_back(42);
	list.push_front(52);
	list.push_front(62);
	list.push_front(72);
	verify(72, *list.at(0), " First element  | 72 ");
	verify(32, *list.at(list.find(32)), " Find element  | 32 "); // this element should be in list so this should be true

	list.pop_front();
	verify(72, *list.at(0), " First element  | 72 ");//should not be true since first elemnt is removed
	verify(62, *list.at(0), " First element  | 62 ");//should be true since first elemnt is removed

	verify(42, *list.at(list.size()-1), " Last element  | 42 ");//should be true
	list.pop_back();
	verify(42, *list.at(list.size() - 1), " Last element  | 42 ");//should not be true since last element was removed
	verify(32, *list.at(list.size() - 1), " Last element  | 32 ");//should be true since last element was removed

	list.printList();
	verify(true, list.clear(), " Clear list | true ");//should be true since there are elements in the list
	verify(0, list.size(), " Size  | 0 "); // should be empty now



}


void test_tree_with_int()
{
	SingelLinkedList<int> list;
	SingelLinkedList<int> list2;
	SearchTree<int> tree;

	verify(0, tree.size(), " Size  | 0 "); // should be empty now

	tree.insert(3);
	tree.insert(1);
	tree.insert(2);
	tree.insert(32);
	tree.insert(11);
	tree.insert(22);
	tree.insert(33);
	tree.insert(21);
	tree.insert(12);

	verify(9, tree.size(), " Size  | 9 "); // should contain nine elements

	verify(true, tree.find(12), " Find Value | 12 "); //should be true since it was inserted
	verify(true, tree.find(14), " Find Value | 14 "); // should be false since no such value has been inserted

	//test traversal in order, filling one list with the numbers in the tree in order. Then comparing it to the list recived from the tree
	list.push_back(1); list.push_back(2); list.push_back(3); list.push_back(11); list.push_back(12);
	list.push_back(21); list.push_back(22); list.push_back(32); list.push_back(33);
	tree.printInOrder(list2);
	list2.printList();
	verify(list, list2, " List in order | same? ");//should be the same
	list2.clear(); list.clear();//clear so it can be used further
	
	//test traversal in pre order, filling one list with the numbers in the tree in order. Then comparing it to the list recived from the tree
	list.push_back(3); list.push_back(1); list.push_back(2); list.push_back(32); list.push_back(11);
	list.push_back(22); list.push_back(21); list.push_back(12); list.push_back(33);
	tree.printPreOrder(list2);
	list2.printList();
	verify(list, list2, " List in Pre order | same? ");//should be the same
	list2.clear(); list.clear();//clear so it can be used further


	//test traversal in post order, filling one list with the numbers in the tree in order. Then comparing it to the list recived from the tree
	list.push_back(2); list.push_back(1); list.push_back(12); list.push_back(21); list.push_back(22);
	list.push_back(11); list.push_back(33); list.push_back(32); list.push_back(3);
	tree.printPostOrder(list2);
	list2.printList();
	verify(list, list2, " List in Post order | same? ");//should be the same
	list2.clear(); list.clear();//clear so it can be used further


	verify(9, tree.size(), " Size  | 9 "); // should contain nine elements
	verify(true, tree.remove(12), " Removal of element  | 12 ");
	verify(true, tree.find(12), " Find element  | 12 ");//should not be found, so this should not be true
	verify(false, tree.find(12), " Find element  | 12 ");//should not be found
	verify(8, tree.size(), " Size  | 8 "); // should contain eight elements

	tree.clear();
	verify(0, tree.size(), " Size  | 0 "); // should not contain any elements

}

int _tmain(int argc, char* argv[])
{

	testSingelwithInt();


	test_tree_with_int();



	char o;
	std::cin >> o;

	return 0;
}

