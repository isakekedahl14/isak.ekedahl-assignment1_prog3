//unit.hpp


#ifndef UNIT_HPP_INCLUDED
#define UNIT_HPP_INCLUDED

#include <iostream>
#include <string>

template <class T>
bool are_equal(T original, T compared)
{
	return original == compared;
}

template <class T>
bool are_equal(SingelLinkedList<T>& original, SingelLinkedList<T>& compared)
{
	if (compared.size() != original.size())
		return false;


	for (int i = 0; i < original.size(); i++)
	{
		int comp = *compared.at(i);
		int org = *original.at(i);

		if (comp != org)
		{
			return false;
		}
	}

	return true;
}


template <class T>
bool verify(T expected, T got, const std::string& message)
{
	if (are_equal(expected, got))
	{
		std::cout << "Success: " << message << std::endl;
		return true;
	}
	std::cout << "Failure: " << message << " Value got: " << got << " Value expected: " << expected << std::endl;
	return false;
}

template <class T>
bool verify(SingelLinkedList<T>& expected, SingelLinkedList<T>& got, const std::string& message)
{
	if (are_equal(expected, got))
	{
		std::cout << "Success: " << message << std::endl;
		return true;
	}
	std::cout << std::endl << "Failure: " << message << std::endl << " Value got: "  << std::endl;
	got.printList();
	std::cout << std::endl << " Value expected: " << std::endl;
	expected.printList();
	std::cout << std::endl;
	return false;
}

#endif //UNIT_HPP_INCLUDED