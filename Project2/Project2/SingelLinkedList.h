#pragma once

#include <stdio.h>
#include <utility>
#include <iostream>

template <class T>
class SingelLinkedList
{
	struct node
	{
		T value;
		node* next;
	};

private:

	node* m_root;
	int m_Size;

public:


	SingelLinkedList()
	{
		m_Size = 0;
		m_root = nullptr;
	};


	~SingelLinkedList()
	{
		if (m_Size != 0 && m_root != nullptr)
		{
			clear();
			m_Size = 0;
			m_root = nullptr;
		}
		
	};

	/*Adds element at first position in the list*/
	bool push_front(T element)
	{
		node* n = new node;
		n->value = element;
		n->next = nullptr;

		if (n == nullptr)
			return false;

		if (m_root != nullptr)
		{
			n->next = m_root;
		}
		
		m_root = n;

		m_Size++;
		return true;
	};

	/*Adds element at the last position in the list*/
	bool push_back(T element)
	{
		node* n = new node;
		n->value = element;
		n->next = nullptr;

		if (m_root == nullptr)
		{
			m_root = n;
		}
		else
		{
			node* m = m_root;
			back(m);

			m->next = n;
		}

		//m_FrontBack.second = n;
		m_Size++;
		return true;
	};

	/*Removes first element if any exists*/
	bool pop_front()
	{
		if (m_root != nullptr)
		{
			node* n = m_root;

			m_root = m_root->next;

			delete n;
			m_Size--;
			return true;
		}

		return false;
	};

	/*Removes last element if any exists*/
	bool pop_back()
	{

		if (m_Size == 0)
		{
			return false;
		}

		node* n = m_root;
		node* m = m_root;
		back(n);
		while (n != m->next)
		{
			m = m->next;
		}

		delete n;
		m->next = nullptr;
		n = nullptr;
		m_Size--;
		return false;
	};

	/*Clears th list of all elements if any exist*/
	bool clear()
	{
		if (m_root == nullptr)
		{
			std::cout << "there is only vacuum here, it cannot be destroyed!!" << std::endl;
			return false;
		}

		destroyAll(m_root);
		m_Size = 0;
		return true;
	}; 

	/*Finds an elemnt value.
	If not found returns -1, else the index of the element*/
	int find(T element)
	{
		node* n = m_root;
		for (int i = 0; i < m_Size; i++)
		{
			if (n->value == element)
			{
				return i;
			}
			if (n->next == nullptr)
			{
				return -1;
			}
			n = n->next;
		}
	};

	/*Returns the element at specified element.
	 If not found returns nullptr*/
	T* at(int index)
	{
		node* n = m_root;
		for (int i = 0; i < m_Size; i++)
		{
			if (i == index)
			{
				return &n->value;
			}

			n = n->next;
		}

		return nullptr;
	}

	/*Returns the number of elemnts in list*/
	int size()
	{
		return m_Size;
	};

	
	/*Writes all the values to the conosole*/
	void printList()
	{
		node* n = m_root;
		if (n != nullptr)
		{
			for (int i = 0; i < m_Size; i++)
			{
				std::cout << (n->value) << " | ";

				n = n->next;
			}

			std::cout <<  std::endl;
		}

	}


	private:
	/*Locates last element in list*/
	void back(node* &n)
	{
		if (n->next != nullptr)
		{
			n = n->next;
			back(n);
		}
	}
	/*iterates through the list and deletes all elements*/
	void destroyAll(node* &n)
	{
		if (n->next != nullptr)
		{
			destroyAll(n->next);
		}
		n->next = nullptr;
		delete n;
		n = nullptr;
	}
};

