#pragma once

#include <stdio.h>
#include <utility>
#include <iostream>
#include "SingelLinkedList.h"

template <class T>
class SearchTree
{

	/*struct node
	{
		T value;
		node* parent;
		node* childLeft;
		node* childRight;
	};
*/

	struct node
	{
		T value;
		node* childLeft;
		node* childRight;
	};

	enum Somthing
	{
		Less,
		More,
		Equalie,
		End,
	};

public:
	SearchTree()
	{
		m_top = nullptr;
	};
	~SearchTree()
	{
		clear();

		m_top = nullptr;
	};


	bool remove(T element)
	{
		if (findOrDestroy(true, element))
		{
			m_size--;
			return true;
		}

		return false;
	};

	bool insert(T element)
	{
		int succesrate = -1;

		node* input = new node();
		input->value = element;
		input->childLeft = nullptr;
		input->childRight = nullptr;

		node* parent = nullptr;

		if (m_top == nullptr)
		{
			m_top = input;
			m_size ++;
			return true;
		}
		else
		{
			node* current = m_top;
			switch (leftRight(element, current, parent))
				{
					case 0:
					{
						current->childLeft = input;
						m_size++;
					}break;
					case 1:
					{
						current->childRight = input;
						m_size++;
					}break;
					case 2:
					{
						current->value = input->value; 
						input->childLeft = nullptr;
						input->childRight = nullptr; 
						delete input;
						
						input = nullptr;
					}break;
					case -2:
					{
						std::cout << "somthing terrible happened, check immediately why? \n" << "Trying to input element though not left or right was a mystery" << std::endl;
						
						input->childLeft = nullptr;
						input->childRight = nullptr;
						delete input;
						
						input = nullptr;
					}break;
				}
			parent = nullptr;
				current = nullptr;
		}

		
		return false;
	};


	bool find(T element)
	{
		return findOrDestroy(false, element);
	};


	bool clear()
	{
		if (m_size != 0)
		{
			if (Clear(m_top))
			{
				return true;
			}
			return false;
		}
		return false;
	};
	int size()
	{
		return m_size;
	}


	void printPreOrder(SingelLinkedList<T>& output)
	{
		traversal_pre_order(output, m_top);
	}
	void printInOrder(SingelLinkedList<T>& output)
	{
		traversal_in_order(output, m_top);

	}
	void printPostOrder(SingelLinkedList<T>& output)
	{
		traversal_post_order(output, m_top);
	}
private:

	bool traversal_pre_order(SingelLinkedList<T>& output, node* &start)
	{
		if (start != nullptr)
		{
			std::cout << " value " << start->value << " node " << start << std::endl;
			output.push_back(start->value);

			if (start->childLeft != nullptr)
			{
				traversal_pre_order(output, start->childLeft);
			}


			if (start->childRight != nullptr)
			{
				traversal_pre_order(output, start->childRight);
			}
		}
		else
		{
			return false;
		}
		return true;
	}

	bool traversal_in_order(SingelLinkedList<T>& output, node* &start)
	{
		if (start != nullptr)
		{
			if (start->childLeft != nullptr)
			{
				traversal_in_order(output, start->childLeft);
			}

			std::cout << " value " << start->value << " node " << start << std::endl;
			output.push_back(start->value);

			if (start->childRight != nullptr)
			{
				traversal_in_order(output, start->childRight);
			}
		}
		else
		{
			return false;
		}
		return true;
	}

	bool traversal_post_order(SingelLinkedList<T>& output, node* &start)
	{
		if (start != nullptr)
		{
			if (start->childLeft != nullptr)
			{
				traversal_post_order(output, start->childLeft);
			}

			if (start->childRight != nullptr)
			{
				traversal_post_order(output, start->childRight);
			}

			std::cout << " value " << start->value << " node " << start << std::endl;
			output.push_back(start->value);
		}
		else
		{
			return false;
		}
		return true;
	}


	bool Clear(node* &start)
	{
		if (start == nullptr)
		{
			return false;
		}

		if (start->childLeft != nullptr)
		{
			Clear(start->childLeft);
		}
		
		if (start->childRight != nullptr)
		{
			Clear(start->childRight);
		}

		start->childLeft = nullptr;
		start->childRight = nullptr;
		delete start;
		start = nullptr;

		return true;
	}

	int greater(T value, node* current)
	{
		if (value > current->value)
		{
			return More;
		}
		else if (value < current->value)
		{
			return Less;
		}
		else if (value == current->value)
		{
			return Equalie;
		}
		
		return -2;
	};


	int leftRight(T value, node* &current, node* &parent)
	{
		int	successrate = -1;
		while (successrate == -1)
		{
			switch (greater(value, current))
			{
			case Less:
			{
				if (current->childLeft)
				{
					parent = current;
					current = current->childLeft;
				}
				else
				{
					successrate = 0;
				}
			}break;
			case More:
			{
				if (current->childRight)
				{
					parent = current;
					current = current->childRight;
				}
				else
				{
					successrate = 1;
				}
			}break;
			case Equalie:
			{
				successrate = 2;
			}break;
			default:
			{
				successrate = -2;
			}break;
			}
		}


		return successrate;
	}

	bool findOrDestroy(bool destroy, T element)
	{
		bool stop = false;
		node* current = m_top;
		node* parent = nullptr;
		if (leftRight(element, current, parent) == 2)
		{
			if (destroy)
			{
				if (current->childRight == nullptr && current->childLeft == nullptr)
				{
					if (parent->childRight == current)
					{
						parent->childRight = nullptr;
					}
					else
					{
						parent->childLeft = nullptr;
					}
					delete current;
				}
				else 
				{
					node* des = current;

					if (current->childRight == nullptr)
					{
						if (parent->childRight == current)
						{
							parent->childRight = current->childLeft;
						}
						else
						{
							parent->childLeft = current->childLeft;
						}
						delete des;
						des = nullptr;
					}
					else
					{
						parent = current;
						current = current->childRight;
						while (current->childLeft != nullptr)
						{
							parent = current;
							current = current->childLeft;
						}
						des->value = current->value;


						if (current->childRight != nullptr)
						{
							parent->childLeft = current->childRight;
							assert(false && "Mayday mayday, solve this quickly");
						}
						
						if (parent->childRight == current)
						{
							parent->childRight = nullptr;
						}
						else
						{
							parent->childLeft = nullptr;
						}
						delete current;
					}
				}
			}
			stop = true;
		}

		parent = nullptr;

		current = nullptr;
		return stop;
	}


	int m_size;
	node* m_top;
};

